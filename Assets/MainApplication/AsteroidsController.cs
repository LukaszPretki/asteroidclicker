using System;
using System.Collections.Generic;
using UnityEngine;

namespace AsteridClicker.MainApplication
{
    public class AsteroidsController
    {
        private readonly List<AsteroidComponent> m_AllAsteroids = new();
        private readonly Action m_ScoreUpdate;

        private const float asteroidDiagonalDimension = 1.73f;// = a*sqrt(3), where "a" is side of cube and it's equal to 1

        public int AsteroidsCount => m_AllAsteroids.Count;

        public AsteroidsController(Action scoreUpdate)
        {
            if (scoreUpdate == null)
                throw new ArgumentNullException(nameof(scoreUpdate), "Score update action can not be null.");
            m_ScoreUpdate = scoreUpdate;
        }

        public void AddAsteroid(AsteroidComponent newAsteroid)
        {
            m_AllAsteroids.Add(newAsteroid);
        }

        private void RemoveAsteroid(AsteroidComponent asteroid)
        {
            m_AllAsteroids.Remove(asteroid);
            GameObject.Destroy(asteroid.gameObject);
        }

        public void FinalAction(AsteroidComponent asteroid)
        {
            m_ScoreUpdate.Invoke();
            RemoveAsteroid(asteroid);
        }

        public bool IsValidPosition(Vector3 newPosition)
        {
            foreach (var asteroid in m_AllAsteroids)
            {
                if (Vector3.Distance(asteroid.transform.position, newPosition) < asteroidDiagonalDimension)
                    return false;
            }
            return true;
        }
    }
}