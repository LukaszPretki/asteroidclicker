using TMPro;
using UnityEngine;

namespace AsteridClicker.MainApplication
{
    public class Main : MonoBehaviour
    {
        [SerializeField]
        private AsteroidComponent m_AsteroidTemplate;
        [SerializeField]
        private TextMeshProUGUI m_ScoreLabel;

        private Spawner m_ObjectSpawner;
        private ScoreManager m_ScoreManager;
        private AsteroidsController m_Controller;

        private void Start()
        {
            Init();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                m_ObjectSpawner.AsteroidInstantiate();
        }

        private void Init()
        {
            m_ScoreManager = new ScoreManager(m_ScoreLabel);
            m_Controller = new AsteroidsController(m_ScoreManager.IncreaseScore);
            m_ObjectSpawner = new Spawner(m_AsteroidTemplate, m_Controller);
            InitScene();
        }

        private void InitScene()
        {
            m_ObjectSpawner.AsteroidInstantiate();
            m_ObjectSpawner.AsteroidInstantiate();
            m_ObjectSpawner.AsteroidInstantiate();
            m_ObjectSpawner.AsteroidInstantiate();
            m_ObjectSpawner.AsteroidInstantiate();
        }
    }
}