using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AsteridClicker.MainApplication
{
    public class AsteroidComponent : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private Renderer m_MeshRenderer;
        private int m_State = -1;
        private readonly Dictionary<int, Action> m_InvokeActionByState = new();

        public void SetFinalAction(Action finalAction)
        {
            m_InvokeActionByState.Add(3, finalAction);
        }

        public void Init()
        {
            m_InvokeActionByState.Add(0, () => m_MeshRenderer.material.color = Color.red);
            m_InvokeActionByState.Add(1, () => m_MeshRenderer.material.color = Color.green);
            m_InvokeActionByState.Add(2, () => m_MeshRenderer.material.color = Color.blue);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            m_State++;
            m_InvokeActionByState[m_State].Invoke();
        }
    }
}