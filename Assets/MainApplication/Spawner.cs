using System;
using UnityEngine;

namespace AsteridClicker.MainApplication
{
    public class Spawner
    {
        private readonly AsteroidComponent m_AsteroidTemplate;
        private readonly AsteroidsController m_AsteroidsController;

        private const float circleRadius = 5.0f;

        public Spawner(AsteroidComponent asteroidTemplate, AsteroidsController controller)
        {
            if (asteroidTemplate == null)
                throw new ArgumentNullException(nameof(asteroidTemplate), "Asteroid Template can not be null.");
            m_AsteroidTemplate = asteroidTemplate;
            if (controller == null)
                throw new ArgumentNullException(nameof(controller), "Asteroids Controller can not be null.");
            m_AsteroidsController = controller;
        }

        public AsteroidComponent AsteroidInstantiate()
        {
            if (m_AsteroidsController.AsteroidsCount > 9)
                return null;

            //Check collision
            var position = UnityEngine.Random.insideUnitCircle * circleRadius;
            while (!m_AsteroidsController.IsValidPosition(position))
                position = UnityEngine.Random.insideUnitCircle * circleRadius;

            var rotation = UnityEngine.Random.rotationUniform;
            var newObj = GameObject.Instantiate(m_AsteroidTemplate, position, rotation);
            newObj.Init();
            newObj.SetFinalAction(() => m_AsteroidsController.FinalAction(newObj));
            m_AsteroidsController.AddAsteroid(newObj);
            return newObj;
        }

    }
}