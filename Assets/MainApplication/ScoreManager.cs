using System;
using TMPro;

namespace AsteridClicker.MainApplication
{
    public class ScoreManager
    {
        private TextMeshProUGUI m_ScoreLabel;

        private int m_Score = 0;
        private const string m_LabelPrefix = "Punkty: ";

        public ScoreManager(TextMeshProUGUI scoreLabel)
        {
            if (scoreLabel == null)
                throw new ArgumentNullException(nameof(scoreLabel), "Score label can not be null.");
            m_ScoreLabel = scoreLabel;
        }

        public void IncreaseScore()
        {
            m_Score++;
            m_ScoreLabel.text = m_LabelPrefix + m_Score.ToString();
        }
    }
}