using NUnit.Framework;
using AsteridClicker.MainApplication;
using Moq;
using UnityEngine;
using System;

public class AsteroidClickerTests
{
    private int m_ActionCallCount;
    private Action m_DefaultAction;
    private AsteroidComponent m_AsteroidComponent;
    private AsteroidsController m_AsteroidController;
    private Spawner m_Spawner;

    [SetUp]
    public void SetUp()
    {
        m_AsteroidComponent = Resources.Load<AsteroidComponent>("Asteroid");
        m_ActionCallCount = 0;
        m_DefaultAction = () => m_ActionCallCount++;
        m_AsteroidController = new AsteroidsController(m_DefaultAction);
        m_Spawner = new Spawner(m_AsteroidComponent, m_AsteroidController);
    }

    [Test]
    public void TestCreateScoreManagerWithNullArgumnet()
    {
        Assert.That(() => new ScoreManager(null), Throws.ArgumentNullException);
    }

    [Test]
    public void TestCreateAsteroidsControllerWithNullArgumnet()
    {
        Assert.That(() => new AsteroidsController(null), Throws.ArgumentNullException);
    }

    [Test]
    public void TestCreateSpawnerWithNullFirstArgumnet()
    {
        Assert.That(() => new Spawner(null, It.IsAny<AsteroidsController>()), Throws.ArgumentNullException);
    }

    [Test]
    public void TestCreateSpawnerWithNullSecondArgumnet()
    {
        Assert.That(() => new Spawner(It.IsAny<AsteroidComponent>(), null), Throws.ArgumentNullException);
    }

    [Test]
    public void TestSpawnMoreThanTenAsteroids()
    {
        m_Spawner.AsteroidInstantiate();//1
        m_Spawner.AsteroidInstantiate();//2
        m_Spawner.AsteroidInstantiate();//3
        m_Spawner.AsteroidInstantiate();//4
        m_Spawner.AsteroidInstantiate();//5
        m_Spawner.AsteroidInstantiate();//6
        m_Spawner.AsteroidInstantiate();//7
        m_Spawner.AsteroidInstantiate();//8
        m_Spawner.AsteroidInstantiate();//9
        m_Spawner.AsteroidInstantiate();//10
        m_Spawner.AsteroidInstantiate();//11

        Assert.AreEqual(m_AsteroidController.AsteroidsCount, 10);
    }

    [Test]
    public void TestFinalActionLaunch()
    {
        var asteroid = m_Spawner.AsteroidInstantiate();
        asteroid.OnPointerClick(null);
        asteroid.OnPointerClick(null);
        asteroid.OnPointerClick(null);
        asteroid.OnPointerClick(null);

        Assert.AreEqual(m_ActionCallCount, 1);
        Assert.AreEqual(m_AsteroidController.AsteroidsCount, 0);
    }


}
